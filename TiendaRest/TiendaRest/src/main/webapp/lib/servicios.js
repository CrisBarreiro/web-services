
$(function () {
    var token = "";
    // Mas info sobre JQuery.ajax en http://api.jquery.com/jquery.ajax/
    $("#get_do").click(function () {
        $.ajax({
            type: "GET",
            headers: {"Authorization": token.toString()},
            url: "api/v1/usuario/" + $("#get_usuario").val(),
            success: function (data, textStatus, response) {
                $("#get_result").val(JSON.stringify(data))
            },
            error: function (response, textStatus, errorThrown) {
                $("#get_result").val("Error: " + textStatus + " " + errorThrown)
            }
        })
    });

    $("#getAll_do").click(function () {
        $.ajax({
            type: "GET",
            headers: {"Authorization": token.toString()},
            url: "api/v1/usuarios?pageNumber=" + $("#get_pageNumber").val() + "&pageSize=" + $("#get_pageSize").val(),
            success: function (data, textStatus, response) {
                $("#getAll_result").val(JSON.stringify(data))
            },
            error: function (response, textStatus, errorThrown) {
                $("#getAll_result").val("Error: " + textStatus + " " + errorThrown)
            }
        })
    });

    $("#delete_do").click(function () {
        $.ajax({
            type: "DELETE",
            headers: {"Authorization": token.toString()},
            url: "api/v1/usuario/" + $("#delete_usuario").val(),
            success: function (data, textStatus, response) {
                $("#delete_result").val(textStatus)
            },
            error: function (response, textStatus, errorThrown) {
                $("#delete_result").val(textStatus + " " + errorThrown)
            }
        })
    });

    $("#deleteAll_do").click(function () {
        $.ajax({
            type: "DELETE",
            headers: {"Authorization": token.toString()},
            url: "api/v1/usuarios",
            success: function (data, textStatus, response) {
                $("#deleteAll_result").val(textStatus)
            },
            error: function (response, textStatus, errorThrown) {
                $("#deleteAll_result").val(textStatus + " " + errorThrown)
            }
        })
    });

    $("#post_do").click(function () {
        var cliente_post = {"mail": $("#post_mail").val(), "nombre": $("#post_nombre").val(),
            "apellidos": $("#post_apellidos").val(), "password": $("#post_password").val(),
            "vip": $("#post_vip").val()}
        $.ajax({
            type: "POST",
            headers: {"Authorization": token.toString()},
            url: "api/v1/usuario",
            data: JSON.stringify(cliente_post),
            contentType: "application/json",
            success: function (data, textStatus, response) {
                $("#post_result").val(JSON.stringify(data) + "\n" + response.getResponseHeader('Location'))
            },
            error: function (response, textStatus, errorThrown) {
                $("#post_result").val("ERROR: " + textStatus + " / " + errorThrown)
            }
        })
    });


    $("#put_do").click(function () {
        var cliente_put = {"mail": $("#put_mail").val(), "nombre": $("#put_nombre").val(),
            "apellidos": $("#put_apellidos").val(), "password": $("#put_password").val()}
        $.ajax({
            type: "PUT",
            headers: {"Authorization": token.toString()},
            url: "api/v1/usuario",
            data: JSON.stringify(cliente_put),
            contentType: "application/json",
            success: function (data, textStatus, response) {
                $("#put_result").val(textStatus)
            },
            error: function (response, textStatus, errorThrown) {
                $("#put_result").val("ERROR: " + textStatus + " / " + errorThrown)
            }
        })
    });


    $("#getCD_do").click(function () {
        $.ajax({
            type: "GET",
            headers: {"Authorization": token.toString()},
            url: "api/v1/cd/" + $("#getCD_codigo").val(),
            success: function (data, textStatus, response) {
                $("#getCD_result").val(JSON.stringify(data))
            },
            error: function (response, textStatus, errorThrown) {
                $("#getCD_result").val("Error: " + textStatus + " " + errorThrown)
            }
        })
    });

    $("#getCD_artista_do").click(function () {
        $.ajax({
            type: "GET",
            headers: {"Authorization": token.toString()},
            url: "api/v1/cds?interprete=" + $("#getCD_artista").val() + "&pageNumber=" + $("#getCD_artista_pageNumber").val() + "&pageSize=" + $("#getCD_artista_pageSize").val(),
            success: function (data, textStatus, response) {
                $("#getCD_artista_result").val(JSON.stringify(data))
            },
            error: function (response, textStatus, errorThrown) {
                $("#getCD_artista_result").val("Error: " + textStatus + " " + errorThrown)
            }
        })
    });

    $("#deleteCD_do").click(function () {
        $.ajax({
            type: "DELETE",
            headers: {"Authorization": token.toString()},
            url: "api/v1/cd/" + $("#deleteCD_codigo").val(),
            success: function (data, textStatus, response) {
                $("#deleteCD_result").val(textStatus)
            },
            error: function (response, textStatus, errorThrown) {
                $("#deleteCD_result").val(textStatus + " " + errorThrown)
            }
        })
    });

    $("#deleteAllCD_do").click(function () {
        $.ajax({
            type: "DELETE",
            headers: {"Authorization": token.toString()},
            url: "api/v1/cds",
            success: function (data, textStatus, response) {
                $("#deleteAllCD_result").val(textStatus)
            },
            error: function (response, textStatus, errorThrown) {
                $("#deleteAllCD_result").val(textStatus + " " + errorThrown)
            }
        })
    });

    $("#postCD_do").click(function () {
        var cd_post = {"titulo": $("#postCD_titulo").val(),
            "interprete": $("#postCD_interprete").val(), "pais": $("#postCD_pais").val(),
            "precio": $("#postCD_precio").val()}
        $.ajax({
            type: "POST",
            headers: {"Authorization": token.toString()},
            url: "api/v1/cd",
            data: JSON.stringify(cd_post),
            contentType: "application/json",
            success: function (data, textStatus, response) {
                $("#postCD_result").val(JSON.stringify(data) + "\n" + response.getResponseHeader('Location'))
            },
            error: function (response, textStatus, errorThrown) {
                $("#postCD_result").val("ERROR: " + textStatus + " / " + errorThrown)
            }
        })
    });


    $("#putCD_do").click(function () {
        var cd_put = {"codigo": $("#putCD_codigo").val(), "titulo": $("#putCD_titulo").val(),
            "interprete": $("#putCD_interprete").val(), "pais": $("#putCD_pais").val(),
            "precio": $("#putCD_precio").val()}
        $.ajax({
            type: "PUT",
            headers: {"Authorization": token.toString()},
            url: "api/v1/cd",
            data: JSON.stringify(cd_put),
            contentType: "application/json",
            success: function (data, textStatus, response) {
                $("#putCD_result").val(textStatus)
            },
            error: function (response, textStatus, errorThrown) {
                $("#putCD_result").val("ERROR: " + textStatus + " / " + errorThrown)
            }
        })
    });
    
        $("#login").click(function () {
        var admin = {"mail": $("#user").val(), "password": $("#password").val()}
        $.ajax({
            type: "POST",
            headers: {"Authorization": token.toString()},
            url: "api/v1/admin",
            data: JSON.stringify(admin),
            contentType: "application/json",
            success: function (data, textStatus, response) {
                token = data;
                $("#login_result").val(data)
            },
            error: function (response, textStatus, errorThrown) {
                $("#login_result").val("ERROR: " + textStatus + " / " + errorThrown)
            }
        })
    });
});