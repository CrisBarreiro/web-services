/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.usc.grei.aos.ejercicio2.ws;

import es.usc.grei.aos.ejercicio2.model.Admin;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 *
 * @author Cristina
 */
public class Authentication {

    private static Map<String, Date> tokens = new HashMap<>();

    public static String generateToken() {
        String randomToken = UUID.randomUUID().toString();
        Date date = new Date();
        tokens.put(randomToken, date);
        return randomToken;
    }
    
    public static Date get(String uuid) {
        if (tokens.containsKey(uuid)) {
            Date date = tokens.get(uuid);
            return date;
        } else {
            return null;
        }      
    }
}
