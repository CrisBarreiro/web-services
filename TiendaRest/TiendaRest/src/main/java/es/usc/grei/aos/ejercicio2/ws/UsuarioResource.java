/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.usc.grei.aos.ejercicio2.ws;

import es.usc.grei.aos.ejercicio2.dao.ClienteDAO;
import es.usc.grei.aos.ejercicio2.model.Cliente;
import es.usc.grei.aos.ejercicio2.model.Page;
import es.usc.grei.aos.ejercicio2.model.WSResult;
import java.net.URI;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author cristina
 */
@Path("/")
public class UsuarioResource {

    @GET
    @Path("usuario/{mail}")
    @Produces(MediaType.APPLICATION_JSON)
    public Cliente getUsuario(@PathParam("mail") String mail) {
        ClienteDAO dao = new ClienteDAO();
        Cliente cliente = dao.getUser(mail);
        if (cliente == null) {
            throw new NotFoundException();
        }
        return cliente;
    }

    @GET
    @Path("usuarios")
    @Produces(MediaType.APPLICATION_JSON)
    public Page getUsuarios(@DefaultValue("0") @QueryParam("pageNumber") int pageNumber, @DefaultValue("5") @QueryParam("pageSize") int pageSize) {
        ClienteDAO dao = new ClienteDAO();
        Page page = new Page();
        page = dao.getAll(pageNumber, pageSize);
        return page;
    }

    @POST
    @Path("usuario")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createUsuario(@Context UriInfo uriInfo, Cliente cliente) {
        ClienteDAO dao = new ClienteDAO();
        System.out.println("Usuario recibido: " + cliente.getMail());
        try {
            dao.create(cliente);
            return Response.created(URI.create(uriInfo.getAbsolutePath().toString() + "/" + cliente.getMail())).entity(new WSResult(true)).build();
        } catch (Exception e) {
            return Response.status(Response.Status.CONFLICT).entity(new WSResult(true)).build();
        }
    }


    @PUT
    @Path("usuario")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateUsuario(@Context UriInfo uriInfo, Cliente cliente) {
        ClienteDAO dao = new ClienteDAO();
        System.out.println("Usuario recibido: " + cliente.getMail());
        try {
            getUsuario(cliente.getMail());
            cliente = dao.update(cliente);
            return Response.ok().entity(new WSResult(true)).build();
        } catch (Exception e) {
            throw new NotFoundException();
        }

    }

    @DELETE
    @Path("usuario/{username}")
    public Response deleteUsuario(@PathParam("username") String username) {
        ClienteDAO dao = new ClienteDAO();
        System.out.println("Usuario recibido: " + username);
        try {
            dao.delete(username);
            return Response.noContent().build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @DELETE
    @Path("usuarios")
    public Response deleteUsuario() {
        ClienteDAO dao = new ClienteDAO();
        try {
            dao.deleteAll();
            return Response.noContent().build();
        } catch (Exception e) {
            throw new NotFoundException();
        }
    }
}
