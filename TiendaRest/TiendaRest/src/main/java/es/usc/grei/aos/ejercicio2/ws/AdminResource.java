/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.usc.grei.aos.ejercicio2.ws;

import es.usc.grei.aos.ejercicio2.dao.AdminDAO;
import es.usc.grei.aos.ejercicio2.model.Admin;
import javax.ws.rs.Consumes;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Cristina
 */
@Path("/")
public class AdminResource {

    @POST
    @Path("admin")
    @Consumes(MediaType.APPLICATION_JSON)
    public String login(@Context UriInfo uriInfo, Admin admin) throws Exception {
        AdminDAO dao = new AdminDAO();
        System.out.println("Usuario recibido: " + admin.getMail());
        try {
            
            if (dao.login(admin) == true) {
                String uuid = Authentication.generateToken();
                return uuid;
            } else {
                throw new NotAuthorizedException("Bad credentials");
            }
        } catch (Exception e) {
            if (e instanceof NotAuthorizedException) {
                throw new NotAuthorizedException(e.getMessage());
            } else {
                throw new NotFoundException();
            } 
        }

    }
}
