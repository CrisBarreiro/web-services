/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.usc.grei.aos.ejercicio2.dao;

import es.usc.grei.aos.ejercicio2.model.Admin;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.NotFoundException;

/**
 *
 * @author Cristina
 */
public class AdminDAO {

    EntityManagerFactory emf = Persistence.createEntityManagerFactory("es.usc.grei.aos_tiendaRest_war_1.0PU");

    public boolean login (Admin admin) {
         EntityManager em = emf.createEntityManager();
         Admin tmp = new Admin();
        try {
            em.getTransaction().begin();
            tmp = (Admin) em.createNamedQuery("Admin.findByMail").setParameter("mail", admin.getMail()).getSingleResult();
            em.getTransaction().commit();
            
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        if (tmp.getPassword().equals(admin.getPassword())) {
            return true;
        } else {
            return false;
        }
    }
    
}
