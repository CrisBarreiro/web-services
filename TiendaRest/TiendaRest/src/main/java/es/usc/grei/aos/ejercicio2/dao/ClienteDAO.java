/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.usc.grei.aos.ejercicio2.dao;

import es.usc.grei.aos.ejercicio2.model.Cliente;
import es.usc.grei.aos.ejercicio2.model.Page;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.NotFoundException;

/**
 *
 * @author Cristina
 */
public class ClienteDAO {

    EntityManagerFactory emf = Persistence.createEntityManagerFactory("es.usc.grei.aos_tiendaRest_war_1.0PU");

    public void create(Cliente cliente) throws Exception {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(cliente);
            em.getTransaction().commit();
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
            em.getTransaction().rollback();
            throw new Exception("User could not be created");
        } finally {
            em.close();
        }
    }

    public Cliente getUser(String email) {
        EntityManager em = emf.createEntityManager();
        Cliente cliente = null;
        try {
            em.getTransaction().begin();
            cliente = (Cliente) em.createNamedQuery("Cliente.findByMail").setParameter("mail", email).getSingleResult();
            em.getTransaction().commit();
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return cliente;
    }

    public Page getAll(int pageNumber, int pageSize) {
        EntityManager em = emf.createEntityManager();
        Page page = new Page();
        page.setPageNumber(pageNumber);
        page.setPageSize(pageSize);
        List<Cliente> clientes = new ArrayList<>();
        long num = 0;
        try {
            em.getTransaction().begin();
            clientes = em.createNamedQuery("Cliente.findAll").setFirstResult(pageSize * pageNumber).setMaxResults(pageSize).getResultList();
            num = (long) em.createNamedQuery("Cliente.getNumber").getSingleResult();
            em.getTransaction().commit();
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
       page.setContent(clientes);
       page.setTotalPages((int) Math.ceil((double) num / (double) pageSize));
       return page;
    }

    public Cliente update(Cliente cliente) throws Exception {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            cliente = em.merge(cliente);
            em.getTransaction().commit();
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
            em.getTransaction().rollback();
            throw new Exception("User could not be updated");
        } finally {
            em.close();
        }
        return cliente;
         
    }
    
    public void delete (String mail) throws Exception {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            if (getUser(mail) == null) {
                throw new NotFoundException();
            }
            em.createNamedQuery("Cliente.delete").setParameter("mail", mail).executeUpdate();
            em.getTransaction().commit();
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
            em.getTransaction().rollback();
            throw new Exception("User not found");
        } finally {
            em.close();
        }
    }
    
    public void deleteAll() throws Exception {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            em.createNamedQuery("Cliente.deleteAll").executeUpdate();
            em.getTransaction().commit();
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
            em.getTransaction().rollback();
            throw new Exception("Table not found");
        } finally {
            em.close();
        }
    }
}
