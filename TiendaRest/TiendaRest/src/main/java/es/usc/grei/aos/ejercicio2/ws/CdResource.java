/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.usc.grei.aos.ejercicio2.ws;

import es.usc.grei.aos.ejercicio2.dao.CdDAO;
import es.usc.grei.aos.ejercicio2.dao.ClienteDAO;
import es.usc.grei.aos.ejercicio2.model.Cd;
import es.usc.grei.aos.ejercicio2.model.Cliente;
import es.usc.grei.aos.ejercicio2.model.Page;
import es.usc.grei.aos.ejercicio2.model.WSResult;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Cristina
 */
@Path("/")
public class CdResource {
    @GET
    @Path("cd/{codigo}")
    @Produces(MediaType.APPLICATION_JSON)
    public Cd getCd(@PathParam("codigo") Long codigo) {
        CdDAO dao = new CdDAO();
        Cd cd = dao.getCd(codigo);
        if (cd == null) {
            throw new NotFoundException();
        }
        return cd;
    }
    
    @GET
    @Path("cds")
    @Produces(MediaType.APPLICATION_JSON)
    public Page getCDs(@QueryParam("interprete") String interprete, @DefaultValue("0") @QueryParam("pageNumber") int pageNumber, @DefaultValue("5") @QueryParam("pageSize") int pageSize) {
        Page page = new Page();
        CdDAO dao = new CdDAO();
        if (!"".equals(interprete)) {
            page = dao.findCdByArtist(interprete, pageNumber, pageSize); 
        } else {
            page = dao.getAll(pageNumber, pageSize);   
        }
        
        return page;
    }
    
    @POST
    @Path("cd")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createCD(@Context UriInfo uriInfo, Cd cd) {
        CdDAO dao = new CdDAO();
        try {
            dao.create(cd);
            return Response.created(URI.create(uriInfo.getAbsolutePath().toString() + "/" + cd.getCodigo())).entity(new WSResult(true)).build();
        } catch (Exception e) {
            return Response.status(Response.Status.CONFLICT).entity(new WSResult(true)).build();
        }
    }
    
    @PUT
    @Path("cd")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateCD(@Context UriInfo uriInfo, Cd cd) {
        CdDAO dao = new CdDAO();
        try {
            getCd(cd.getCodigo());
            cd = dao.update(cd);
            return Response.ok().entity(new WSResult(true)).build();
        } catch (Exception e) {
            throw new NotFoundException();
        }

    }
    
    @DELETE
    @Path("cd/{codigo}")
    public Response deleteCD(@PathParam("codigo") Long codigo) {
        CdDAO dao = new CdDAO();
        try {
            dao.delete(codigo);
            return Response.noContent().build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
    
    @DELETE
    @Path("cds")
    public Response deleteCDs() {
        CdDAO dao = new CdDAO();
        try {
            dao.deleteAll();
            return Response.noContent().build();
        } catch (Exception e) {
            throw new NotFoundException();
        }
    }
}
