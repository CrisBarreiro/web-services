/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.usc.grei.aos.ejercicio2.dao;

import es.usc.grei.aos.ejercicio2.model.Cd;
import es.usc.grei.aos.ejercicio2.model.Page;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.NotFoundException;

/**
 *
 * @author Cristina
 */
public class CdDAO {

    EntityManagerFactory emf = Persistence.createEntityManagerFactory("es.usc.grei.aos_tiendaRest_war_1.0PU");

    public void create(Cd cd) throws Exception {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(cd);
            em.getTransaction().commit();
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
            em.getTransaction().rollback();
            throw new Exception("User could not be created");
        } finally {
            em.close();
        }
    }

    public Cd getCd(Long codigo) {
        EntityManager em = emf.createEntityManager();
        Cd cd = null;
        try {
            em.getTransaction().begin();
            cd = (Cd) em.createNamedQuery("Cd.findByCodigo").setParameter("codigo", codigo).getSingleResult();
            em.getTransaction().commit();
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return cd;
    }
    
        public Page findCdByArtist(String artist, int pageNumber, int pageSize) {
        EntityManager em = emf.createEntityManager();
        Page page = new Page();
        List<Cd> cds = new ArrayList<>();
        long num = 0;
        try {
            em.getTransaction().begin();
            cds = em.createNamedQuery("Cd.findByInterprete").setFirstResult(pageSize * pageNumber).setMaxResults(pageSize).setParameter("interprete", artist).getResultList();
            num = (long) em.createNamedQuery("Cd.getNumberByArtist").setParameter("interprete", artist).getSingleResult();
            em.getTransaction().commit();
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        page.setPageNumber(pageNumber);
        page.setPageSize(pageSize);
        page.setTotalPages((int) Math.ceil((double) num / (double) pageSize));
        page.setContent(cds);
        return page;
    }

    public Page getAll(int pageNumber, int pageSize) {
        EntityManager em = emf.createEntityManager();
        Page page = new Page();
        List<Cd> cds = new ArrayList<>();
        long num = 0;
        try {
            em.getTransaction().begin();
            cds = em.createNamedQuery("Cd.findAll").setFirstResult(pageSize * pageNumber).setMaxResults(pageSize).getResultList();
            num = (long) em.createNamedQuery("Cd.getNumber").getSingleResult();
            em.getTransaction().commit();
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        page.setPageNumber(pageNumber);
        page.setPageSize(pageSize);
        page.setTotalPages((int) Math.ceil((double) num / (double) pageSize));
        page.setContent(cds);
        return page;
    }

    public Cd update(Cd cd) throws Exception {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            cd = em.merge(cd);
            em.getTransaction().commit();
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
            em.getTransaction().rollback();
            throw new Exception("Cd could not be updated");
        } finally {
            em.close();
        }
        return cd;
         
    }
    
    public void delete (Long codigo) throws Exception {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            if (getCd(codigo) == null) {
                throw new NotFoundException();
            }
            em.createNamedQuery("Cd.delete").setParameter("codigo", codigo).executeUpdate();
            em.getTransaction().commit();
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
            em.getTransaction().rollback();
            throw new Exception("User not found");
        } finally {
            em.close();
        }
    }
    
    public void deleteAll() throws Exception {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            em.createNamedQuery("Cd.deleteAll").executeUpdate();
            em.getTransaction().commit();
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }
}
