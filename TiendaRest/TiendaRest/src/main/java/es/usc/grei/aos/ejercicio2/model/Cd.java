/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.usc.grei.aos.ejercicio2.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Cristina
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cd.findAll", query = "SELECT c FROM Cd c"),
    @NamedQuery(name = "Cd.findByCodigo", query = "SELECT c FROM Cd c WHERE c.codigo = :codigo"),
    @NamedQuery(name = "Cd.findByTitulo", query = "SELECT c FROM Cd c WHERE c.titulo = :titulo"),
    @NamedQuery(name = "Cd.findByInterprete", query = "SELECT c FROM Cd c WHERE c.interprete = :interprete"),
    @NamedQuery(name = "Cd.findByPais", query = "SELECT c FROM Cd c WHERE c.pais = :pais"),
    @NamedQuery(name = "Cd.findByPrecio", query = "SELECT c FROM Cd c WHERE c.precio = :precio"),
    @NamedQuery(name = "Cd.delete", query = "DELETE FROM Cd c WHERE c.codigo = :codigo"),
    @NamedQuery(name = "Cd.deleteAll", query = "DELETE FROM Cd c"),
    @NamedQuery(name = "Cd.getNumber", query = "SELECT count(c.codigo) FROM Cd c"),
    @NamedQuery(name = "Cd.getNumberByArtist", query = "SELECT count(c.codigo) FROM Cd c WHERE c.interprete = :interprete")}
)
public class Cd implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Long codigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    private String titulo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    private String interprete;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    private String pais;
    @Basic(optional = false)
    @NotNull
    private float precio;

    public Cd() {
    }

    public Cd(Long codigo) {
        this.codigo = codigo;
    }

    public Cd(Long codigo, String titulo, String interprete, String pais, float precio) {
        this.codigo = codigo;
        this.titulo = titulo;
        this.interprete = interprete;
        this.pais = pais;
        this.precio = precio;
    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getInterprete() {
        return interprete;
    }

    public void setInterprete(String interprete) {
        this.interprete = interprete;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigo != null ? codigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cd)) {
            return false;
        }
        Cd other = (Cd) object;
        if ((this.codigo == null && other.codigo != null) || (this.codigo != null && !this.codigo.equals(other.codigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.usc.grei.aos.ejercicio2.model.Cd[ codigo=" + codigo + " ]";
    }

}
