package es.usc.grei.aos.ejercicio2.filters;

import es.usc.grei.aos.ejercicio2.ws.Authentication;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;

@Provider
public class DemoRequestFilter implements ContainerRequestFilter {

    @Override
    public void filter(ContainerRequestContext cr) throws IOException {
        String auth = cr.getHeaderString("Authorization");
        UriInfo uriInfo = cr.getUriInfo();
        String uri = uriInfo.getAbsolutePath().getPath();
        Date now = new Date();
        long limit = 50000;
        long diff = 0;
        if (!uri.equals("/tiendarest/api/v1/admin")) {

            if (auth.isEmpty()) {
                throw new WebApplicationException(Response.Status.UNAUTHORIZED);
            } else {
                try {
                    Date before = Authentication.get(auth);
                    if (before == null) {
                        throw new WebApplicationException(Response.Status.UNAUTHORIZED);
                    } else {
                        diff = now.getTime() - before.getTime();
                        if (diff > limit) {
                            throw new WebApplicationException(Response.Status.UNAUTHORIZED);
                        }
                    }
                } catch (Exception e) {
                    throw new WebApplicationException(Response.Status.UNAUTHORIZED);
                }
            }
        }
    }
}
