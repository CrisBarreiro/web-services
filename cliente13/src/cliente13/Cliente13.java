/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente13;

import busqueda.BusquedaAutor;
import busqueda.BusquedaID;
import catalogo.CatalogoClient;
import catalogo.ListaProductos;
import catalogo.Producto;
import java.util.Scanner;
import login.Usuario;
import login.loginClient;
import org.nocrala.tools.texttablefmt.Table;

/**
 *
 * @author crist_000
 */
public class Cliente13 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Usuario usuario = null;
        Scanner scanner = new Scanner(System.in);;
        while (usuario == null) {
            System.out.println("Introduzca su correo electrónico: ");
            String user = scanner.next();
            System.out.println("Introduzca su contraseña: ");
            String pass = scanner.next();
            Usuario usuarioRecibido = new Usuario();
            usuarioRecibido.setMail(user);
            usuarioRecibido.setPassword(pass);
            System.out.println("Comprobando datos de inicio de sesión...");
            usuario = loginClient.comprobarLogin(usuarioRecibido);
            if (usuario != null) {
                if (usuario.isAdmin()) {
                    System.out.println("Bienvenido a su cuenta de administrador, " + usuario.getMail());
                } else {
                    System.out.println("Bienvenido a su cuenta de cliente, " + usuario.getMail());
                }
            } else {
                System.out.println("Usuario y/o contraseña incorrectos. Por favor, inténtelo otra vez");
            }
        }

        /*El usuario ya está logueado*/
        ListaProductos lista = CatalogoClient.getPage(1, Integer.MAX_VALUE);

        Table t = new Table(5);
        t.addCell("Código");
        t.addCell("Album");
        t.addCell("Intérprete");
        t.addCell("País");
        t.addCell("Precio");
        for (Producto producto : lista.getProductos()) {
            t.addCell(producto.getCodigo());
            t.addCell(producto.getTitulo());
            t.addCell(producto.getInterprete());
            t.addCell(producto.getPais());
            t.addCell(String.format("%.2f", producto.getPrecio()) + " €");
        }
        System.out.println(t.render());

        System.out.println("Introduzca campo de búsqueda:\n 1. código\n 2. intérprete");
        scanner = new Scanner(System.in);
        char option = scanner.next().charAt(0);
        switch (option) {
            case '1':
                System.out.println("Introduzca el código del producto a buscar");
                String codigo = scanner.next();
                busquedaID.Producto prod = BusquedaID.busquedaID(codigo);
                if (prod != null) {
                    t = new Table(5);
                    t.addCell("Código");
                    t.addCell("Album");
                    t.addCell("Intérprete");
                    t.addCell("País");
                    t.addCell("Precio");
                    t.addCell(prod.getCodigo());
                    t.addCell(prod.getTitulo());
                    t.addCell(prod.getInterprete());
                    t.addCell(prod.getPais());
                    t.addCell(String.format("%.2f", prod.getPrecio()) + " €");
                    System.out.println(t.render());
                } else {
                    System.out.println("Ningún producto coindice con el criterio de búsqueda");
                }
                break;
            case '2':
                System.out.println("Introduzca el nombre del intérprete");
                String autor = scanner.next();
                busquedaAutor.ListaProductos listaProductos = BusquedaAutor.buscar(autor);
                if (!listaProductos.getProductos().isEmpty()) {
                    t = new Table(5);
                    t.addCell("Código");
                    t.addCell("Album");
                    t.addCell("Intérprete");
                    t.addCell("País");
                    t.addCell("Precio");
                    for (busquedaAutor.Producto producto : listaProductos.getProductos()) {
                        t.addCell(producto.getCodigo());
                        t.addCell(producto.getTitulo());
                        t.addCell(producto.getInterprete());
                        t.addCell(producto.getPais());
                        t.addCell(String.format("%.2f", producto.getPrecio()) + " €");
                    }
                    System.out.println(t.render());
                } else {
                    System.out.println("Ningún producto coindice con el criterio de búsqueda seleccionado");
                }

                break;
            default:
                System.out.println("Opción incorrecta");
        }
    }

}
