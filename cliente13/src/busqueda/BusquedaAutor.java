/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package busqueda;

import busquedaAutor.ListaProductos;

/**
 *
 * @author crist_000
 */
public class BusquedaAutor {

    public static ListaProductos buscar(java.lang.String autor) {
        busquedaAutor.BusquedaAutor_Service service = new busquedaAutor.BusquedaAutor_Service();
        busquedaAutor.BusquedaAutor port = service.getBusquedaAutorPort();
        return port.buscar(autor);
    }  
}
