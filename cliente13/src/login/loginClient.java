/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login;

/**
 *
 * @author crist_000
 */
public class loginClient {

    public static Usuario comprobarLogin(login.Usuario usuario) {
        login.Login_Service service = new login.Login_Service();
        login.Login port = service.getLoginPort();
        return port.comprobarLogin(usuario);
    }

    public static boolean isAdmin(login.Usuario usuario) {
        login.Login_Service service = new login.Login_Service();
        login.Login port = service.getLoginPort();
        return port.isAdmin(usuario);
    }
    
}
