/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menu;
import cliente.AnalizadorTextos;
import java.util.Scanner;

/**
 *
 * @author crist_000
 */
public class MenuAnalizadorTextos {

    public MenuAnalizadorTextos() {
        System.out.println("Introduzca una opción:\n 1. Contar número de palabras de un texto\n 2. Contar número de caracteres de un texto\n 3. Obtener el número de repeticiones de una palabra\n 4. Obtener la palabra con más repeticiones\n 5. Salir");
        Scanner scanner = new Scanner(System.in);
        int option = scanner.nextInt();
        if (option >= 1 && option <= 4) {
            System.out.println("Introduzca un texto y pulse ENTER para finalizar");
            scanner = new Scanner (System.in);
            String text = scanner.nextLine();
            int resultado;
            switch (option) {
                case 1:
                    resultado = AnalizadorTextos.wordCount(text);
                    System.out.println("El texto tiene " + resultado + " palabras");
                    break;
                case 2:
                    resultado = AnalizadorTextos.charCount(text);
                    System.out.println("El texto tiene " + resultado + " caracteres");
                    break;
                case 3:
                    System.out.println("Introduzca la palabra a buscar");
                    String word = scanner.next();
                    resultado = AnalizadorTextos.wordFreq(text, word);
                    System.out.println("La palabra " + word + " se encontró " + resultado + " veces");
                    break;
                case 4:
                    String result = AnalizadorTextos.maxFreq(text);
                    System.out.println("La palabra más frecuente es " + result + " se encontró " + AnalizadorTextos.wordFreq(text, result) + " veces");
                    break;        
            }
        } else if (option == 0) {
            System.exit(0);
        } else {
            System.out.println("El valor introducido no es correcto.");
        }
    }
    
}
