/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menu;

import cliente.CalculadoraAvanzada;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author crist_000
 */
public class MenuCalculadoraAvanzada {

    public MenuCalculadoraAvanzada() {
        System.out.println("Introduzca una opción:\n"
                + " 1. Maximo\n"
                + " 2. Minimo\n"
                + " 3. Media\n"
                + " 0. Salir");
        Scanner scanner = new Scanner(System.in);
        int option = scanner.nextInt();
        if (option >= 1 && option <= 3) {
            System.out.println("Introduzca una lista de números separados por espacio (en caso de introducir decimales, deben separarse mediante punto). Pulse Enter para finalizar la introducción");
            try {
                scanner = new Scanner(System.in);
                String cadena = scanner.nextLine();
                ArrayList<String> listaString = new ArrayList(Arrays.asList(cadena.split(" ")));
                ArrayList<Float> lista = new ArrayList<Float>();
                for (String numero : listaString) {
                    lista.add(Float.parseFloat(numero));
                }
                Float resultado = new Float(0);
                switch (option) {
                    case 1:
                        resultado = CalculadoraAvanzada.maximo(lista);
                        break;
                    case 2:
                        resultado = CalculadoraAvanzada.minimo(lista);
                        break;
                    case 3: 
                        resultado = CalculadoraAvanzada.media(lista);
                        break;
                }
                if (resultado != null) {
                    System.out.println("El resultado es: " + resultado);
                } else {
                    System.out.println("Se produjo un error al calcular el resultado");
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

        }
    }

}
