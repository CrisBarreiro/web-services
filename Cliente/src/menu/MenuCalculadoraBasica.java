/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menu;

import calculadoraBasica.DivisionPorCeroException_Exception;
import cliente.CalculadoraBasica;
import java.util.Scanner;

/**
 *
 * @author crist_000
 */
public class MenuCalculadoraBasica {
    
    public MenuCalculadoraBasica() {
        while (true) {
            System.out.println("Escoja una operación: \n 1. Suma\n 2. Resta\n 3. Multiplicacion\n 4. Division\n 0. Salir");
            Scanner scanner = new Scanner(System.in);
            int option = scanner.nextInt();
            Float resultado = null;
            Float operando1;
            Float operando2;
            if (option > 0 && option < 5) {
                System.out.println("Introduzca el primer operando: ");
                operando1 = scanner.nextFloat();
                System.out.println("Introduzca el segundo operando: ");
                operando2 = scanner.nextFloat();
                switch (option) {
                    case 1:
                        resultado = CalculadoraBasica.suma(operando1, operando2);
                        break;
                    case 2:
                        resultado = CalculadoraBasica.resta(operando1, operando2);
                        break;
                    case 3:
                        resultado = CalculadoraBasica.multiplicacion(operando1, operando2);
                        break;
                    case 4:
                        try {
                            resultado = CalculadoraBasica.division(operando1, operando2);
                        } catch (DivisionPorCeroException_Exception e){
                            System.out.println(e.getFaultInfo().getMessage());
                        }
                        
                        break;
                }
                if (resultado != null) {
                    System.out.println("El resultado es: " + resultado);
                }
            } else if (option == 0){
                System.exit(0);
            }
        }
    }
}
