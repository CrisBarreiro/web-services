/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente;

/**
 *
 * @author crist_000
 */
public class calculadoraBasica {

    public static Float division(java.lang.Float dividendo, java.lang.Float divisor) {
        interfaz.CalculadoraBasica_Service service = new interfaz.CalculadoraBasica_Service();
        interfaz.CalculadoraBasica port = service.getCalculadoraBasicaPort();
        return port.division(dividendo, divisor);
    }

    public static Float multiplicacion(java.lang.Float factor1, java.lang.Float factor2) {
        interfaz.CalculadoraBasica_Service service = new interfaz.CalculadoraBasica_Service();
        interfaz.CalculadoraBasica port = service.getCalculadoraBasicaPort();
        return port.multiplicacion(factor1, factor2);
    }

    public static Float resta(java.lang.Float minuendo, java.lang.Float sustraendo) {
        interfaz.CalculadoraBasica_Service service = new interfaz.CalculadoraBasica_Service();
        interfaz.CalculadoraBasica port = service.getCalculadoraBasicaPort();
        return port.resta(minuendo, sustraendo);
    }

    public static Float suma(java.lang.Float sumando1, java.lang.Float sumando2) {
        interfaz.CalculadoraBasica_Service service = new interfaz.CalculadoraBasica_Service();
        interfaz.CalculadoraBasica port = service.getCalculadoraBasicaPort();
        return port.suma(sumando1, sumando2);
    }
    
}
