/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente;

import java.util.Scanner;
import menu.MenuAnalizadorTextos;
import menu.MenuCalculadoraAvanzada;
import menu.MenuCalculadoraBasica;
import menu.MenuConversorMonedas;

/**
 *
 * @author crist_000
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        while (true) {
            System.out.println("Bienvenido al ejercicio 1.1, qué herramienta desea utilizar?"
                    + "\n 1. Calculadora básica"
                    + "\n 2. Calculadora avanzada"
                    + "\n 3. Analizador de textos"
                    + "\n 4. Conversor de monedas"
                    + "\n 0. Salir");
            Scanner scanner = new Scanner(System.in);
            int option = scanner.nextInt();
            if (option >= 1 && option <=4) {
                switch (option) {
                    case 1:
                        new MenuCalculadoraBasica();
                        break;
                    case 2:
                        new MenuCalculadoraAvanzada();
                        break;
                    case 3:
                        new MenuAnalizadorTextos();
                        break;
                    case 4:
                        new MenuConversorMonedas();
                        break;
                }
            } else if (option == 0) {
                System.exit(0);
            } else {
                System.out.println("Opción incorrecta, por favor, vuelva a introducir una opción");
            }

        }

    }
}
