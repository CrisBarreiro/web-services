/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente;

/**
 *
 * @author crist_000
 */
public class AnalizadorTextos {

    public static int charCount(java.lang.String text) {
        analizadorTextos.AnalizadorTextos_Service service = new analizadorTextos.AnalizadorTextos_Service();
        analizadorTextos.AnalizadorTextos port = service.getAnalizadorTextosPort();
        return port.charCount(text);
    }

    public static String maxFreq(java.lang.String text) {
        analizadorTextos.AnalizadorTextos_Service service = new analizadorTextos.AnalizadorTextos_Service();
        analizadorTextos.AnalizadorTextos port = service.getAnalizadorTextosPort();
        return port.maxFreq(text);
    }

    public static int wordCount(java.lang.String text) {
        analizadorTextos.AnalizadorTextos_Service service = new analizadorTextos.AnalizadorTextos_Service();
        analizadorTextos.AnalizadorTextos port = service.getAnalizadorTextosPort();
        return port.wordCount(text);
    }

    public static int wordFreq(java.lang.String text, java.lang.String word) {
        analizadorTextos.AnalizadorTextos_Service service = new analizadorTextos.AnalizadorTextos_Service();
        analizadorTextos.AnalizadorTextos port = service.getAnalizadorTextosPort();
        return port.wordFreq(text, word);
    }
    
}
