/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente;

/**
 *
 * @author crist_000
 */
public class CalculadoraAvanzada {

    public static Float maximo(java.util.List<java.lang.Float> lista) {
        calculadoraAvanzada.CalculadoraAvanzada_Service service = new calculadoraAvanzada.CalculadoraAvanzada_Service();
        calculadoraAvanzada.CalculadoraAvanzada port = service.getCalculadoraAvanzadaPort();
        return port.maximo(lista);
    }

    public static Float media(java.util.List<java.lang.Float> lista) {
        calculadoraAvanzada.CalculadoraAvanzada_Service service = new calculadoraAvanzada.CalculadoraAvanzada_Service();
        calculadoraAvanzada.CalculadoraAvanzada port = service.getCalculadoraAvanzadaPort();
        return port.media(lista);
    }

    public static Float minimo(java.util.List<java.lang.Float> lista) {
        calculadoraAvanzada.CalculadoraAvanzada_Service service = new calculadoraAvanzada.CalculadoraAvanzada_Service();
        calculadoraAvanzada.CalculadoraAvanzada port = service.getCalculadoraAvanzadaPort();
        return port.minimo(lista);
    }
    
}
