/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente;

/**
 *
 * @author crist_000
 */
public class ConversorMonedas {

    public static double conversionRate(conversorMoneda.Currency fromCurrency, conversorMoneda.Currency toCurrency) {
        conversorMoneda.CurrencyConvertor service = new conversorMoneda.CurrencyConvertor();
        conversorMoneda.CurrencyConvertorSoap port = service.getCurrencyConvertorSoap();
        return port.conversionRate(fromCurrency, toCurrency);
    }
    
}
