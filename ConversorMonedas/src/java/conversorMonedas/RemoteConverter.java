/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversorMonedas;

/**
 *
 * @author crist_000
 */
public class RemoteConverter {

    public static double conversionRate(remoteConverter.Currency fromCurrency, remoteConverter.Currency toCurrency) {
        remoteConverter.CurrencyConvertor service = new remoteConverter.CurrencyConvertor();
        remoteConverter.CurrencyConvertorSoap port = service.getCurrencyConvertorSoap();
        return port.conversionRate(fromCurrency, toCurrency);
    }
    
}
