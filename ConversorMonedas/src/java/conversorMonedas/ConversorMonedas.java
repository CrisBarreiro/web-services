/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversorMonedas;

import java.util.ArrayList;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import remoteConverter.Currency;

/**
 *
 * @author crist_000
 */
@WebService(serviceName = "ConversorMonedas")
public class ConversorMonedas {

    /**
     * Web service operation
     */
    @WebMethod(operationName = "convert")
    public List<Float> convert(@WebParam(name = "listaPrecios") List<Float> listaPrecios, @WebParam(name = "monedaOrigen") Currency monedaOrigen, @WebParam(name = "monedaDestino") Currency monedaDestino) {
        double factor = RemoteConverter.conversionRate(monedaOrigen, monedaDestino);
        List<Float> preciosFinales = new ArrayList<>();
        for (Float precio : listaPrecios) {
            preciosFinales.add((float)(precio * factor));
        }
        return preciosFinales;
    }
}
