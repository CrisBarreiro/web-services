/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exception;

import javax.xml.ws.WebFault;

/**
 *
 * @author crist_000
 */
@WebFault (name = "DivisionPorCeroException")
public class DivisionPorCeroException extends Exception{

    public DivisionPorCeroException() {
        super("Error: No es posible dividir por cero");
    }
    
}
