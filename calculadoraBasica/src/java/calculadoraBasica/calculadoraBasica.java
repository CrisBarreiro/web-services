/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadoraBasica;

import exception.DivisionPorCeroException;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author crist_000
 */
@WebService(serviceName = "calculadoraBasica")
public class calculadoraBasica {

    /**
     * Web service operation
     */
    @WebMethod(operationName = "suma")
    public Float suma(@WebParam(name = "sumando1") Float sumando1, @WebParam(name = "sumando2") Float sumando2) {
        return sumando1 + sumando2;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "resta")
    public Float resta(@WebParam(name = "minuendo") Float minuendo, @WebParam(name = "sustraendo") Float sustraendo) {
        return minuendo - sustraendo;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "multiplicacion")
    public Float multiplicacion(@WebParam(name = "factor1") Float factor1, @WebParam(name = "factor2") Float factor2) {
        return factor1 * factor2;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "division")
    public Float division(@WebParam(name = "dividendo") Float dividendo, @WebParam(name = "divisor") Float divisor) throws DivisionPorCeroException {
        if (divisor != 0) {
            return dividendo / divisor;
        }
        else {
            throw new DivisionPorCeroException();
        }
    }
}
