/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadoraAvanzada;

import java.util.Collections;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author crist_000
 */
@WebService(serviceName = "CalculadoraAvanzada")
public class CalculadoraAvanzada {

    /**
     * Web service operation
     */
    @WebMethod(operationName = "maximo")
    public Float maximo(@WebParam(name = "lista") List<Float> lista) {
        return Collections.max(lista);
    }

    public Float minimo(@WebParam(name = "lista") List<Float> lista) {
        return Collections.min(lista);
    }

    public Float media(@WebParam(name = "lista") List<Float> lista) {
        float total = 0;
        for (float tmp : lista) {
            total += tmp;
        }
        return total/lista.size();
    }
}
