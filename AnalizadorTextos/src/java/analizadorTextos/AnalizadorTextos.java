/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analizadorTextos;

import static com.sun.org.apache.xalan.internal.lib.ExsltStrings.split;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author crist_000
 */
@WebService(serviceName = "AnalizadorTextos")
public class AnalizadorTextos {

    /**
     * Devuelve el número de palabras de un texto
     */
    @WebMethod(operationName = "wordCount")
    public int wordCount(@WebParam(name = "text") String text) {
        return text.split(" ").length;
    }

    /**
     * Devuelve el número de caracteres de un texto
     */
    @WebMethod(operationName = "charCount")
    public int charCount(@WebParam(name = "text") String text) {
        return text.length();
    }

    /**
     * Devuelve la frecuencia de una determinada palabra
     */
    @WebMethod(operationName = "wordFreq")
    public int wordFreq(@WebParam(name = "text") String text, @WebParam(name = "word") String word) {
        int number = 0;
        ArrayList<String> list = new ArrayList(Arrays.asList(text.split(" ")));
        for (String tmp : list) {
            if (word.equals(tmp)) {
                number++;
            }
        }
        return number;
    }

    /**
     * Busca la palabra con la mayor frecuencia
     */
    @WebMethod(operationName = "maxFreq")
    public String maxFreq(@WebParam(name = "text") String text) {
        ArrayList<String> list = new ArrayList(Arrays.asList(text.split(" ")));
        HashMap<String, Integer> freq = new HashMap<String, Integer>();
        for (String tmp : list) {
            if (!freq.containsKey(tmp)) {
                freq.put(tmp, 1);
            } else {
                freq.put(tmp, freq.get(tmp) + 1);
            }
        }

        Iterator itor = freq.entrySet().iterator();
        String maxKey = "";
        int maxValue = 0;
        while (itor.hasNext()) {
            Entry<String, Integer> et = (Entry) itor.next();
            if (et.getValue() > maxValue) {
                maxKey = et.getKey();
                maxValue = et.getValue();
            }
        }
        return maxKey;
    }
}
