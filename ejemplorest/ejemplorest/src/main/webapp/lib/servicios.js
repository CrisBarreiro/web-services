
$(function () {

    // Mas info sobre JQuery.ajax en http://api.jquery.com/jquery.ajax/
        $("#get_do").click(function () {
        $.ajax({
            type: "GET",
            headers: {"Authorization": "cristina"},
            url: "api/v1/usuario/"+$("#get_usuario").val(),
            success: function (data, textStatus, response) {
                $("#get_result").val(JSON.stringify(data))
            },
            error: function (response, textStatus, errorThrown) {
                $("#get_result").val("Error: " + textStatus + " " + errorThrown)
            }
        })
    });
    
            $("#getAll_do").click(function () {
        $.ajax({
            type: "GET",
            headers: {"Authorization": "cristina"},
            url: "api/v1/usuarios?pageNumber="+$("#get_pageNumber").val()+"&pageSize="+$("#get_pageSize").val(),
            success: function (data, textStatus, response) {
                $("#getAll_result").val(JSON.stringify(data))
            },
            error: function (response, textStatus, errorThrown) {
                $("#getAll_result").val("Error: " + textStatus + " " + errorThrown)
            }
        })
    });

        $("#delete_do").click(function () {
        $.ajax({
            type: "DELETE",
            headers: {"Authorization": "cristina"},
            url: "api/v1/usuario/"+$("#delete_usuario").val(),
            success: function (data, textStatus, response) {
                $("#delete_result").val(textStatus)
            },
            error: function (response, textStatus, errorThrown) {
                $("#delete_result").val(textStatus + " " + errorThrown)
            }
        })
    });
    
    $("#deleteAll_do").click(function () {
        $.ajax({
            type: "DELETE",
            headers: {"Authorization": "cristina"},
            url: "api/v1/usuarios",
            success: function (data, textStatus, response) {
                $("#deleteAll_result").val(textStatus)
            },
            error: function (response, textStatus, errorThrown) {
                $("#deleteAll_result").val(textStatus + " " + errorThrown)
            }
        })
    });

    $("#post_do").click(function () {
        var usuario_post = {"username": $("#post_usuario").val(), "nombre": $("#post_nombre").val(), 
            "apellidos": $("#post_apellidos").val(), "edad": $("#post_edad").val(),
            "sexo": $("#post_sexo").val()}
        $.ajax({
            type: "POST",
            headers: {"Authorization": "cristina"},
            url: "api/v1/usuario",
            data: JSON.stringify(usuario_post),
            contentType: "application/json",
            success: function (data, textStatus, response) {
                $("#post_result").val(JSON.stringify(data)+"\n"+response.getResponseHeader('Location'))
            },
            error: function (response, textStatus, errorThrown) {
                $("#post_result").val("ERROR: " + textStatus + " / " + errorThrown)
            }
        })
    });
    
    
        $("#put_do").click(function () {
        var usuario_put = {"username": $("#put_usuario").val(), "nombre": $("#put_nombre").val(), 
            "apellidos": $("#put_apellidos").val(), "edad": $("#put_edad").val(),
            "sexo": $("#put_sexo").val()}
        $.ajax({
            type: "PUT",
            headers: {"Authorization": "cristina"},
            url: "api/v1/usuario",
            data: JSON.stringify(usuario_put),
            contentType: "application/json",
            success: function (data, textStatus, response) {
                $("#put_result").val(textStatus)
            },
            error: function (response, textStatus, errorThrown) {
                $("#put_result").val("ERROR: " + textStatus + " / " + errorThrown)
            }
        })
    });
});