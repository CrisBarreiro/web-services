/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.usc.grei.aos.ejercicio1.ws;

import es.usc.grei.aos.ejercicio1.model.Datos;
import es.usc.grei.aos.ejercicio1.model.Page;
import es.usc.grei.aos.ejercicio1.model.Usuario;
import es.usc.grei.aos.ejercicio1.model.WSResult;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author cristina
 */
@Path("/")
public class UsuarioResource {

    @GET
    @Path("usuario/{username}")
    @Produces(MediaType.APPLICATION_JSON)
    public Usuario getUsuario(@PathParam("username") String username) {
        Usuario usuario = Datos.getUsuario(username);
        if (usuario == null) {
            throw new NotFoundException();
        }
        return usuario;
    }

    @GET
    @Path("usuarios")
    @Produces(MediaType.APPLICATION_JSON)
    public Page getUsuarios(@DefaultValue("0") @QueryParam("pageNumber") int pageNumber, @DefaultValue("5") @QueryParam("pageSize") int pageSize) {
        Page page = new Page();
        List<Usuario> listaUsuarios = new ArrayList(Datos.getListaUsuarios().values());
        page.setPageNumber(pageNumber);
        page.setPageSize(pageSize);
        page.setTotalPages((int) Math.ceil((double)listaUsuarios.size() / (double)pageSize));
        if (pageNumber < page.getTotalPages()) {
            page.setContent(listaUsuarios.subList(pageSize * pageNumber, Math.min(pageSize * pageNumber + pageSize, listaUsuarios.size())));
        } else {
            listaUsuarios = new ArrayList<>();
            page.setContent(listaUsuarios);
        }
        
        return page;
    }
    
    @POST
    @Path("usuario")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createUsuario(@Context UriInfo uriInfo, Usuario usuario) {
        System.out.println("Usuario recibido: " + usuario.getUsername());
        if (Datos.nuevoUsuario(usuario)) {
            return Response.created(URI.create(uriInfo.getAbsolutePath().toString() + "/" + usuario.getUsername())).entity(new WSResult(true)).build();
        } else {
            return Response.status(Response.Status.CONFLICT).entity(new WSResult(true)).build();
        }

    }

    @PUT
    @Path("usuario")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateUsuario(@Context UriInfo uriInfo, Usuario usuario) {
        System.out.println("Usuario recibido: " + usuario.getUsername());
        if (Datos.modificarUsuario(usuario)) {
            return Response.ok().entity(new WSResult(true)).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).entity(new WSResult(true)).build();
        }

    }

    @DELETE
    @Path("usuario/{username}")
    public Response deleteUsuario(@PathParam("username") String username) {
        System.out.println("Usuario recibido: " + username);
        if (Datos.eliminarUsuario(username)) {
            return Response.noContent().build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

    }

    @DELETE
    @Path("usuarios")
    public Response deleteUsuario() {
        Datos.deleteAll();
        return Response.noContent().build();
    }

}
