package es.usc.grei.aos.ejercicio1.filters;

import java.io.IOException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

@Provider
public class DemoRequestFilter implements ContainerRequestFilter {

    @Override
    public void filter(ContainerRequestContext cr) throws IOException {
        String auth = cr.getHeaderString("Authorization");
        if (auth == null || !auth.equals("cristina")) {
            throw new WebApplicationException(Response.Status.UNAUTHORIZED);
        }
    }
}
