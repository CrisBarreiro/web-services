/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.usc.grei.aos.ejercicio1.model;

import java.util.HashMap;

/**
 *
 * @author cristina
 */
public class Datos {

    private static HashMap<String, Usuario> listaUsuarios;

    static {
        listaUsuarios = new HashMap<>();
        listaUsuarios.put("chustuan", new Usuario("chustuan", "Christian", "Toimil Arango", 21, 'H'));
        listaUsuarios.put("varo", new Usuario("varo", "Álvaro", "Brey Vilas", 21, 'H'));
        listaUsuarios.put("stelixa", new Usuario("stelixa", "Stela", "Domínguez Ferreira", 21, 'M'));
        listaUsuarios.put("nanaStrange", new Usuario("nanaStrange", "Ana", "Rilo Guevara", 21, 'M'));
        listaUsuarios.put("estiven", new Usuario("estiven", "Esteban", "Rozas Liñares", 21, 'H'));
        listaUsuarios.put("adri", new Usuario("adri", "Adrián", "Rodríguez Vilas", 20, 'H'));
        listaUsuarios.put("porto", new Usuario("porto", "Álvaro", "Porto Ares", 21, 'H'));
    }

    public static HashMap<String, Usuario> getListaUsuarios() {
        return listaUsuarios;
    }

    public static boolean nuevoUsuario(Usuario usuario) {
        if (!listaUsuarios.containsKey(usuario.getUsername())) {
            listaUsuarios.put(usuario.getUsername(), usuario);
            return true;
        } else {
            return false;
        }

    }

    public static boolean modificarUsuario(Usuario usuario) {
        if (listaUsuarios.containsKey(usuario.getUsername())) {
            listaUsuarios.put(usuario.getUsername(), usuario);
            return true;
        } else {
            return false;
        }
    }

    public static boolean eliminarUsuario(String usuario) {
        if (listaUsuarios.containsKey(usuario)) {
            listaUsuarios.remove(usuario);
            return true;
        } else {
            return false;
        }
    }

    public static Usuario getUsuario(String username) {
        if (listaUsuarios.containsKey(username)) {
            return listaUsuarios.get(username);
        } else {
            return null;
        }

    }

    public static void deleteAll() {
        listaUsuarios = new HashMap<>();
    }

}
