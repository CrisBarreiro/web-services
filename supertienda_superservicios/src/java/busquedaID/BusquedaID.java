/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package busquedaID;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import modelo.DAOProducto;
import modelo.ListaProductos;
import modelo.Producto;

/**
 *
 * @author crist_000
 */
@WebService(serviceName = "BusquedaID")
public class BusquedaID {

    /**
     * Web service operation
     */
    @WebMethod(operationName = "busquedaID")
    public Producto busquedaID(@WebParam(name = "id") String id) {
        DAOProducto dao = new DAOProducto();
        Producto producto = new Producto();
        producto = dao.readCd(id);
        return producto;
    }
}
