/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import modelo.Admin;
import modelo.Cliente;
import modelo.DAOUsuario;
import modelo.Usuario;

/**
 *
 * @author crist_000
 */
@WebService(serviceName = "Login")
public class Login {

    /**
     * Web service operation
     */
    @WebMethod(operationName = "comprobarLogin")
    public Usuario comprobarLogin(@WebParam(name = "usuario") Usuario usuario) {
        DAOUsuario dao = new DAOUsuario();
        Usuario ret = dao.comprobarLogin(usuario);
        ret = (ret.isAdmin() ? (Admin) ret : (Cliente) ret);
        return ret;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "isAdmin")
    public boolean isAdmin(@WebParam(name = "usuario") Usuario usuario) {
        return usuario.isAdmin();
    }
}
