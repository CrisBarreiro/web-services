/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package catalogo;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import modelo.DAOProducto;
import modelo.ListaProductos;

/**
 *
 * @author crist_000
 */
@WebService(serviceName = "Catalogo")
public class Catalogo {

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getPage")
    public ListaProductos getPage(@WebParam(name = "page") Integer page, Integer pageSize) {
        DAOProducto dao = new DAOProducto();
        int inicio = (page - 1) * pageSize;
        ListaProductos lista = dao.readList(inicio, pageSize);
        lista.setNumeroPagina(page);
        return lista;
    }
}
