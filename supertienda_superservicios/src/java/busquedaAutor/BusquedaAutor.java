/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package busquedaAutor;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import modelo.DAOProducto;
import modelo.ListaProductos;
import modelo.Producto;

/**
 *
 * @author crist_000
 */
@WebService(serviceName = "BusquedaAutor")
public class BusquedaAutor {

    /**
     * Web service operation
     */
    @WebMethod(operationName = "buscar")
    public ListaProductos buscar(@WebParam(name = "autor") String autor) {
        DAOProducto dao = new DAOProducto();
        Producto producto = new Producto();
        producto.setInterprete(autor);
        ListaProductos lista = dao.readSearch(producto, 0, Integer.MAX_VALUE);
        return lista;
    }
}
