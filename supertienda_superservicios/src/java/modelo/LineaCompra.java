package modelo;

import java.util.*;

public class LineaCompra {

    public LineaCompra() {
        this.cd = new Producto();
        this.cantidad = 0;
    }

    public LineaCompra(Producto cd, Integer cantidad) {
        this.cd = cd;
        this.cantidad = cantidad;
    }

    private Producto cd;
    private Integer cantidad;

    public Producto getCd() {
        return cd;
    }

    public void setCd(Producto cd) {
        this.cd = cd;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

}
