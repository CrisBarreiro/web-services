package modelo;

public class Usuario {

    public Usuario() {
    }

    public Usuario(String mail, String password) {
        this.nombre = "";
        this.apellidos = "";
        this.mail = mail;
        this.password = password;

    }
    protected String nombre;
    protected String apellidos;
    protected String mail;
    protected String password;
    protected boolean admin;

    public String getApellidos() {
        return apellidos;
    }

    public String getMail() {
        return mail;
    }

    public String getNombre() {
        return nombre;
    }

    public String getPassword() {
        return password;
    }

    public boolean isAdmin() {
        return admin;
    }
    
    
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

}
