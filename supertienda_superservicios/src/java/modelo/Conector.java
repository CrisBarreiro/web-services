package modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conector {

    private Connection conexion;

    public Conector() {
        conexion = null;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }

    private void conectar() {
        try {
            conexion = DriverManager.getConnection("jdbc:mysql://localhost/supertienda?user=root");
        } catch (SQLException ex) {
            System.out.println("Error al conectar a la base de datos" + ex.getMessage());
        }
    }

    public void desconectar() {
        try {
            conexion.close();
            conexion = null;
        } catch (SQLException ex) {
            System.out.println("Error al desconectar de la base de datos" + ex.getMessage());
        }
    }

    public Connection getConexion() {
        if (conexion == null) {
            this.conectar();
        }
        return conexion;
    }

}
