package modelo;

import java.util.*;

public class ListaProductos {

    public ListaProductos() {
    }

    private List<Producto> productos;
    private Integer numeroPagina;

    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }

    public Integer getNumeroPagina() {
        return numeroPagina;
    }

    public void setNumeroPagina(Integer numeroPagina) {
        this.numeroPagina = numeroPagina;
    }

}
