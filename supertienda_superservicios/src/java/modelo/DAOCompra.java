package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DAOCompra {

    public DAOCompra() {
    }

    public synchronized void realizarCompra(Carrito carrito, Cliente cliente) {
        Conector conector = new Conector();
        Connection conexion = conector.getConexion();
        PreparedStatement consulta = null;
        ResultSet resultado = null;
        String id = "";
        try {
            conexion.setAutoCommit(false);
            consulta = conexion.prepareStatement("INSERT INTO pedidos (precio_pedido, descuento, cliente) VALUES (?, ?, ?)");
            consulta.setFloat(1, carrito.getPrecioTotal());
            consulta.setFloat(2, carrito.getDescuento());
            consulta.setString(3, cliente.getMail());
            consulta.execute();
            consulta = conexion.prepareStatement("SELECT id_pedido FROM pedidos ORDER BY id_pedido desc LIMIT 1");
            resultado = consulta.executeQuery();
            if (resultado.next()) {
                id = resultado.getString("id_pedido");
            }
            for (LineaCompra linea : carrito.getLineasCompra()) {
                consulta = conexion.prepareStatement("INSERT INTO lineascompra VALUES (?, ?, ?, ?, ?)");
                consulta.setString(1, id);
                consulta.setString(2, linea.getCd().getCodigo());
                consulta.setInt(3, linea.getCantidad());
                consulta.setFloat(4, linea.getCd().getPrecio());
                consulta.setFloat(5, linea.getCd().getPrecio() * linea.getCantidad());
                consulta.execute();
                consulta = conexion.prepareStatement("UPDATE stock SET stock = stock - ? WHERE cd LIKE ?");
                consulta.setInt(1, linea.getCantidad());
                consulta.setString(2, linea.getCd().getCodigo());
                consulta.execute();
            }
            conexion.commit();

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {

            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                }
                if (consulta != null && !consulta.isClosed()) {
                    consulta.close();
                }
                if (resultado != null && !resultado.isClosed()) {
                    resultado.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DAOCompra.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public synchronized Float totalGastado(Cliente cliente) {
        Conector conector = new Conector();
        Connection conexion = conector.getConexion();
        PreparedStatement consulta = null;
        ResultSet resultado = null;
        Float totalGastado = (float) 0.0;
        try {
            consulta = conexion.prepareStatement("SELECT SUM(precio_pedido) AS total FROM pedidos WHERE cliente LIKE ?");
            consulta.setString(1, cliente.getMail());
            resultado = consulta.executeQuery();
            if (resultado.next()) {
                totalGastado = resultado.getFloat("total");
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {

            try {
                if (conexion != null && !conexion.isClosed()) {
                    conexion.close();
                }
                if (consulta != null && !consulta.isClosed()) {
                    consulta.close();
                }
                if (resultado != null && !resultado.isClosed()) {
                    resultado.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DAOCompra.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return totalGastado;
    }
}
