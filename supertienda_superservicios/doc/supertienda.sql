-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-04-2015 a las 13:27:52
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `supertienda`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `nombre` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `mail` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(25) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `admin`
--

INSERT INTO `admin` (`nombre`, `apellidos`, `mail`, `password`) VALUES
('Al', 'Guapi', 'a@a.a', '1234'),
('Pepe', 'Potamo Garcia', 'pepepotamo@gmail.com', '1234');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cd`
--

CREATE TABLE IF NOT EXISTS `cd` (
`codigo` bigint(20) unsigned NOT NULL,
  `titulo` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `interprete` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `pais` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `precio` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `cd`
--

INSERT INTO `cd` (`codigo`, `titulo`, `interprete`, `pais`, `precio`) VALUES
(2, 'AEnima', 'Tool', 'USA', 20.99),
(3, 'Pedrá', 'Extremoduro', 'Spain', 14.99),
(4, 'In Rainbows', 'Radiohead', 'United Kingdom', 10),
(5, 'Stadium Arcadium', 'Red Hot Chili Peppers', 'USA', 12.99),
(6, 'I Love Your Glasses', 'Russian Red', 'Spain', 17.99),
(7, 'The Blue Album', 'Weezer', 'USA', 22.5),
(8, 'House of Gold and Bones', 'Stone Sour', 'USA', 15.95),
(9, 'In The Aeroplane Over The Sea', 'Neutral Milk Hotel', 'USA', 25),
(10, 'Ten', 'Pearl Jam', 'USA', 16),
(11, 'Lateralus', 'Tool', 'USA', 15.99),
(12, 'Rearviewmirror', 'Pearl Jam', 'USA', 25),
(13, 'Brothers', 'The Black Keys', 'USA', 16),
(14, 'In Absentia', 'Porcupine Tree', 'United Kingdom', 17.99),
(15, 'El Sitio De Mi Recreo', 'Antonio Vega', 'Spain', 20),
(16, 'The King of Limbs', 'Radiohead', 'United Kingdom', 24.99),
(17, 'Angels', 'The Strokes', 'USA', 18.99),
(18, 'Riders On The Storm', 'The Doors', 'USA', 15),
(19, 'Poligamia', 'Los Piratas', 'Spain', 12.99),
(20, 'Only By The Night', 'Kings of Leon', 'USA', 16),
(21, '2112', 'Rush', 'Canada', 15.25);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE IF NOT EXISTS `clientes` (
  `nombre` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `mail` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `vip` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`nombre`, `apellidos`, `mail`, `password`, `vip`) VALUES
('Álvaro', 'Brey Vilas', 'alvaro.brv@gmail.com', '1234', 1),
('Cris', 'Sanchez', 'cristina.sbarreiro@gmail.com', 'guapi', 0),
('Foo', 'Bar', 'foo@bar.com', '1234', 0),
('Pepe', 'Palote', 'ppalote@p.palo', 'ppalote', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

CREATE TABLE IF NOT EXISTS `comentarios` (
  `cliente` varchar(30) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `cd` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comentario` text COLLATE utf8_spanish_ci NOT NULL,
  `valoracion` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lineascompra`
--

CREATE TABLE IF NOT EXISTS `lineascompra` (
  `pedido` bigint(20) unsigned NOT NULL DEFAULT '0',
  `producto` bigint(20) unsigned NOT NULL DEFAULT '0',
  `cantidad` int(11) NOT NULL,
  `precio` float NOT NULL,
  `precioTotal` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `lineascompra`
--

INSERT INTO `lineascompra` (`pedido`, `producto`, `cantidad`, `precio`, `precioTotal`) VALUES
(1, 4, 2, 10, 20),
(1, 5, 1, 12.99, 12.99),
(2, 3, 1, 14.99, 14.99),
(2, 5, 1, 12.99, 12.99),
(2, 6, 1, 17.99, 17.99),
(3, 5, 1, 12.99, 12.99),
(3, 6, 1, 17.99, 17.99),
(4, 2, 2, 20.99, 41.98),
(4, 3, 2, 14.99, 29.98),
(4, 4, 5, 10, 50),
(4, 5, 4, 12.99, 51.96),
(4, 6, 3, 17.99, 53.97),
(4, 7, 35, 22.5, 787.5),
(5, 7, 1, 22.5, 22.5),
(6, 7, 1, 22.5, 22.5),
(7, 7, 1, 22.5, 22.5),
(8, 9, 1, 25, 25),
(10, 7, 1, 22.5, 22.5),
(11, 9, 1, 25, 25),
(12, 5, 1, 12.99, 12.99),
(13, 7, 2, 22.5, 45),
(14, 2, 1, 20.99, 20.99),
(14, 3, 1, 14.99, 14.99),
(14, 4, 1, 10, 10),
(14, 5, 1, 12.99, 12.99),
(14, 6, 1, 17.99, 17.99),
(14, 7, 1, 22.5, 22.5),
(14, 9, 1, 25, 25),
(15, 7, 1, 22.5, 22.5),
(16, 5, 3, 12.99, 38.97),
(16, 9, 1, 25, 25),
(17, 5, 1, 12.99, 12.99),
(18, 3, 1, 14.99, 14.99),
(18, 5, 3, 12.99, 38.97),
(18, 6, 1, 17.99, 17.99);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE IF NOT EXISTS `pedidos` (
`id_pedido` bigint(20) unsigned NOT NULL,
  `precio_pedido` float DEFAULT NULL,
  `descuento` float NOT NULL,
  `cliente` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `pedidos`
--

INSERT INTO `pedidos` (`id_pedido`, `precio_pedido`, `descuento`, `cliente`) VALUES
(1, 32.99, 0, 'alvaro.brv@gmail.com'),
(2, 45.97, 0, 'alvaro.brv@gmail.com'),
(3, 30.98, 0, 'alvaro.brv@gmail.com'),
(4, 1015.39, 0, NULL),
(5, 22.5, 0, NULL),
(6, 22.5, 4.5, NULL),
(7, 22.5, 0, NULL),
(8, 25, 0, 'alvaro.brv@gmail.com'),
(9, 0, 0, 'alvaro.brv@gmail.com'),
(10, 22.5, 0, 'alvaro.brv@gmail.com'),
(11, 25, 5, 'alvaro.brv@gmail.com'),
(12, 12.99, 2.598, 'alvaro.brv@gmail.com'),
(13, 45, 9, 'alvaro.brv@gmail.com'),
(14, 124.46, 24.892, 'alvaro.brv@gmail.com'),
(15, 22.5, 4.5, 'alvaro.brv@gmail.com'),
(16, 63.97, 0, 'cristina.sbarreiro@gmail.com'),
(17, 12.99, 2.598, 'alvaro.brv@gmail.com'),
(18, 71.95, 14.39, 'alvaro.brv@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stock`
--

CREATE TABLE IF NOT EXISTS `stock` (
  `cd` bigint(20) unsigned NOT NULL DEFAULT '0',
  `stock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `stock`
--

INSERT INTO `stock` (`cd`, `stock`) VALUES
(2, 19),
(3, 5),
(4, 7),
(5, 0),
(6, 8),
(7, 7),
(9, 6),
(10, 15),
(11, 10),
(12, 5),
(13, 16),
(14, 20),
(15, 10),
(16, 30),
(17, 6),
(18, 15),
(19, 5),
(20, 12),
(21, 9);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admin`
--
ALTER TABLE `admin`
 ADD PRIMARY KEY (`mail`);

--
-- Indices de la tabla `cd`
--
ALTER TABLE `cd`
 ADD PRIMARY KEY (`codigo`), ADD UNIQUE KEY `codigo` (`codigo`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
 ADD PRIMARY KEY (`mail`);

--
-- Indices de la tabla `comentarios`
--
ALTER TABLE `comentarios`
 ADD PRIMARY KEY (`cliente`,`cd`), ADD KEY `comentarios_ibfk_2` (`cd`);

--
-- Indices de la tabla `lineascompra`
--
ALTER TABLE `lineascompra`
 ADD PRIMARY KEY (`pedido`,`producto`), ADD KEY `lineascompra_ibfk_2` (`producto`);

--
-- Indices de la tabla `pedidos`
--
ALTER TABLE `pedidos`
 ADD PRIMARY KEY (`id_pedido`), ADD UNIQUE KEY `id_pedido` (`id_pedido`), ADD KEY `cliente` (`cliente`), ADD KEY `cliente_2` (`cliente`);

--
-- Indices de la tabla `stock`
--
ALTER TABLE `stock`
 ADD PRIMARY KEY (`cd`), ADD UNIQUE KEY `cd` (`cd`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cd`
--
ALTER TABLE `cd`
MODIFY `codigo` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `pedidos`
--
ALTER TABLE `pedidos`
MODIFY `id_pedido` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `comentarios`
--
ALTER TABLE `comentarios`
ADD CONSTRAINT `comentarios_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`mail`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `comentarios_ibfk_2` FOREIGN KEY (`cd`) REFERENCES `cd` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `lineascompra`
--
ALTER TABLE `lineascompra`
ADD CONSTRAINT `lineascompra_ibfk_1` FOREIGN KEY (`pedido`) REFERENCES `pedidos` (`id_pedido`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `lineascompra_ibfk_2` FOREIGN KEY (`producto`) REFERENCES `cd` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `pedidos`
--
ALTER TABLE `pedidos`
ADD CONSTRAINT `pedidos_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`mail`) ON DELETE SET NULL ON UPDATE CASCADE,
ADD CONSTRAINT `pedidos_ibfk_2` FOREIGN KEY (`id_pedido`) REFERENCES `pedidos` (`id_pedido`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `stock`
--
ALTER TABLE `stock`
ADD CONSTRAINT `stock_ibfk_1` FOREIGN KEY (`cd`) REFERENCES `cd` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
