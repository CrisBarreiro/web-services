/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientemonedacaracteres;

import java.util.ArrayList;
import java.util.List;
import net.webservicex.Currency;

/**
 *
 * @author crist_000
 */
public class ClienteMonedaCaracteres {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String texto = "miau miau miau miau miau miau miau miau miau miau miau"
                + "miau miau miau miau miau miau miau miau miau miau miau miau"
                + "miau miau miau miau miau miau miau miau miau miau miau miau";
        System.out.println("Van a contarse los caracteres del siguiente texto " + texto);
        System.out.println("El texto tiene " + AnalizadorTextos.charCount(texto) + " caracteres");
        
        List<Float> precios = new ArrayList<>();
        precios.add(19.90f);
        precios.add(25.50f);
        precios.add(12.60f);
        precios.add(67.25f);
        try {
            List<Float> preciosConvertidos = ConversorMonedas.convert(precios, Currency.valueOf("EUR"), Currency.valueOf("USD"));
            int i = 0;
            for (Float precio : precios) {
                System.out.println(precio + " euros = " + preciosConvertidos.get(i) + " dólares");
                i++;
            }
        } catch (Exception e){
            System.out.println("Se ha producido un error" + e.getMessage());
        }
        
    }
    
}
