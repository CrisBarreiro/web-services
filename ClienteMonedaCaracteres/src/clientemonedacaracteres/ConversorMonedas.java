/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientemonedacaracteres;

/**
 *
 * @author crist_000
 */
public class ConversorMonedas {

    public static java.util.List<java.lang.Float> convert(java.util.List<java.lang.Float> listaPrecios, net.webservicex.Currency monedaOrigen, net.webservicex.Currency monedaDestino) {
        conversormonedas.ConversorMonedas_Service service = new conversormonedas.ConversorMonedas_Service();
        conversormonedas.ConversorMonedas port = service.getConversorMonedasPort();
        return port.convert(listaPrecios, monedaOrigen, monedaDestino);
    }
    
}
